#!/usr/bin/python

import urllib3
import json
import httplib2
import urllib
import sys
import xmlrpclib
import os
from bs4 import BeautifulSoup
#from PM_email import EMAILLIST

try:
    from urllib.request  import urlopen
except ImportError:
    from urllib import urlopen

def mkdir(path):
    path = path.strip()
    path = path.rstrip("\\")
    isExists = os.path.exists(path)

    if not isExists:
        #print path
        print path + ' create successfully!'
        os.makedirs(path)
        return True
    else:
        print path + ' exists!'
        return False

def makeTableContentList(table):
    result = []
    allrows = table.findAll('tr')
    rowIndex = 0
    for row in allrows:
        result.append([])
        if row.findAll('s'):
                continue

        allcols = row.findAll('td')
        #print "rowIndex = ",rowIndex
        #print "allcols = ",allcols

        for col in allcols:
            #print "col",col
            thestrings = [unicode(s) for s in col.findAll(text=True)]
            thetext = ''.join(thestrings)

            result[-1].append(thetext)
        rowIndex += 1
    return result

def makeFile(private,tableContentList):
    #print tableContentList
    className = tableContentList[0]
    outputFile = open(private,'w')

    # write header
    #outputFile.write("using UnityEngine;\n")
    #outputFile.write("using System.Collections;\n\n")
    #print(className)
    #outputFile.write(str(className) + "\n{\n")

    # write members
    rowCounter = 0
    print(tableContentList)
    for row in tableContentList:
        if row and rowCounter > 0 and len(row) > 1:
            #------------format------------
            typeString = format(row[0]) + ','
            memberName = format(row[14])+','
            comments = ""

            if len(row[17]) > 1:
                comments = format(row[17]) + ','

            s = memberName + typeString + comments + "\n"
            outputFile.write(s)
        rowCounter += 1

    #write tail
    #outputFile.write("}\n")

    # close file
    outputFile.close()

def setDefaultEncodingUTF8():
    reload(sys)
    sys.setdefaultencoding('utf-8')


def getConf():
    '''http://confluence.alaudatech.com/rest/api/content/31918107'''
    get_url = "http://confluence.alaudatech.com/rest/api/content/31918107?expand=body.storage"
    login_url = "http://confluence.alaudatech.com/dologin.action"
    username = "cnlu"
    password = "Alauda1234"

    h = httplib2.Http(".cache")

    headers = {'Content-type': 'application/x-www-form-urlencoded'}
    body = {'os_username': username, 'os_password': password}
    resp, content = h.request(
        uri=login_url,
        method='POST',
        headers=headers,
        body=urllib.urlencode(body)
    )
    cookies = resp.get("set-cookie")
    headers = {'Cookie': cookies}
    response, content = h.request(get_url, 'GET', headers=headers)
    obj = json.loads(content)
    html = obj['body']['storage']['value']
    soup = BeautifulSoup( html,'html5lib' )
    tables = soup.findAll('table')
    setDefaultEncodingUTF8()
    mkdir(sys.path[0] + "/output")

    for table in tables:
        #print table
        result = makeTableContentList(table)
        makeFile("./private",result)
        #print "result = "
        #print result


if __name__ == '__main__':
    getConf()