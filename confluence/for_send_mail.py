#!/usr/bin/python
# -*- coding: utf-8 -*-
# coding: LuChunNing

# import utils
import time
from email import encoders
from email.header import Header
from email.mime.text import MIMEText
from email.utils import parseaddr, formataddr
import smtplib

#smtpconfig = {
#    'provider': 'office365',
#    'username': 'timesheet@alauda.io',
#    'password': 'Gongshi12345',
#    'from': 'timesheet@alauda.io'
#}

#cc_reciver
#acc = ['htwang@alauda.io' , 'cnlu@alauda.io' , 'xlliu@alauda.io' ,'hbqi@alauda.io']

#date
#time1=time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))

def _format_addr(s):
    name, addr = parseaddr(s)
    return formataddr(( \
        Header(name, 'utf-8').encode(), \
        addr.encode('utf-8') if isinstance(addr, unicode) else addr))

def read_whole_text(filename):
    text = ""
    file = open(filename, 'r')
    try:
        text = file.read()
    finally:
        file.close()
    return text


def parse_line(line):
    try:
        l = line.split(',')
        # print l
        info = {
            "employee": l[0],
            "email": l[1],
            "project": l[2],
            "content": l[3]
        }
        return info
    except Exception as e:
        return None


def read_infos():
    subject = read_whole_text('for_send_mail.subject')
    print subject
    if subject == "":
        return (0, "", "", "", "")

    body = read_whole_text('for_send_mail.body')
    #print body
    if body == "":
        return (0, "", "", "", "")

    all_infos = {}
    file = open('./private', 'r')
    try:
        while True:
            line = file.readline()
            if line:
                info = parse_line(line.strip())
                if info:
                    if info["employee"] not in all_infos:
                        all_infos[info["employee"]] = []
                    all_infos[info["employee"]].append(info)
                else:
                    print "Error in parsing: " + line
            else:
                break
    finally:
        file.close()
    # print all_infos
    return (1, subject, body, all_infos)



def send_email():
    (ret, subject, body, all_infos) = read_infos()
    if not ret:
        return 0

    # print subject
    # print body
    table_header = "<table style='border-collapse:collapse; border:1px solid blue; border-spacing:5px 5px; border-spacing:2px;' ><tr><th>项目经理</th><th>项目</th><th>运维建议</th></tr>"
    table_footer = "</table>"
    # print all_infos
    nums = 0

    for employee, info_list in all_infos.items():
        content = ""
        email_address = ""
        for info in info_list:
            email_address = info["email"]
            content = content + "<tr><td>{0}</td><td>{1}</td><td>{2}</td></tr>".format(info["employee"], info["project"], info["content"])

        content = table_header + content + table_footer   #table_header

        email_body = body.replace("[employee]", employee).replace("[content]", content)
        email_body = email_body.replace("\n", "<br/>")
        email_body = """<html>
                     <style>
                        th {border: 1px solid black; padding: 5px; background-color:#ddd;}
                        td {border: 1px solid black; padding: 5px;}
                     </style>
                     <body>""" + email_body + "</body></html>"

        server = smtplib.SMTP("outlook.office365.com", 587)  # SMTP协议默认端口是25
        server.set_debuglevel(1)
        server.ehlo()  # 向Gamil发送SMTP 'ehlo' 命令
        server.starttls()
        server.login("cnlu@alauda.io", "Luchunning888")

        from_addr = "cnlu@alauda.io"
        to_addr = email_address
        #cc_addr = cc_reciver
        #cc_addr = cc_reciver
        msg = MIMEText(email_body, 'html', 'utf-8')
        msg['From'] = _format_addr(u'cnlu编写者 <%s>' % from_addr)
        msg['To'] = _format_addr(u'send发送者 <%s>' % to_addr)
        #msg['Cc'] = acc
        msg['Subject'] = Header(u'私有项目隐患信息', 'utf-8').encode()

        #print "email_address:" , email_address
        #exit(0)
        server.sendmail("cnlu@alauda.io", [email_address], msg.as_string()) # + acc
        server.quit()
        nums = nums + 1
        #print email_body
        time.sleep(1)
    return nums

if __name__ == "__main__":
    nums = send_email()
    print "send all nums {}".format(nums)