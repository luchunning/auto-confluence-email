#python代码简介
python执行环境 2.7.15以上（除3.0版本以上）

#python代码作用
用户自动获取confluence 私有项目信息，自动发送邮件

#目录介绍：
1）confluence目录为存放代码的目录
2）automatic_confluence.py为获取confluence执行页面信息
3）for_send_mail.py 为自动发邮件
4）for_send_mail.subject 和 for_send_mail.body为邮件的头部和主题
5）email_robot.sh类似于综合上述代码的脚本，可作为crontab使用
